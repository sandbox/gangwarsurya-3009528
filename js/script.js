/**
 * @file
 * Js file data for import form.
 */
(function ($, Drupal, drupalSettings) {
      $('.subject-list').on('change', function() {
          $('.subject-list').not(this).prop('checked', false);
      });
})(jQuery, Drupal, drupalSettings);