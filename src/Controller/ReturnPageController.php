<?php

namespace Drupal\registration_subscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class ReturnPageController.
 *
 * @package Drupal\registration_subscription\Controller
 */
class ReturnPageController extends ControllerBase {

  /**
   * Page which shows if donation was successful.
   *
   * @return string
   *   Returns HTML string.
   */
  public function success(Request $request) {
    $custom = $request->request->get('custom');
    //kint($name); exit;
    
    if (!empty($_POST['custom'])) {
      //echo 877898; 
      $custom = explode('@@@', $_POST['custom']);
     // print_r($custom); exit;
      $subscription_id = $custom['0'];
      $subscription_user_id = $custom['1'];
      $mc_gross = $_POST['mc_gross'];
      //Get Package Information
      if (!empty($subscription_user_id) && !empty($subscription_id)) {
        print_r($subscription_user_id);
        print_r($subscription_id);
        $package_info = \Drupal::entityTypeManager()->getStorage('subscription')
              ->loadByProperties(['id' => $subscription_id]);
        $package_info = reset($package_info);
        
        kint($package_info); exit;
        if (isset($package_info->subscription_amount) ) {
          $subscription_amount = $package_info->get('subscription_amount')->getValue()[0]['value'];
        }
        // verify subscription amount and paypal return amount
        if ($mc_gross == $subscription_amount) {
          $data = [
            'payment_status' => 1,
            'status'         => 1,
            'changed'        => time(),
          ]; 
          $this->updateUserSubscription($data, $subscription_id, $subscription_user_id);
        } 
      }
    }
    return [
      '#type' => 'markup',
      '#markup' => $this->config('paypal.settings')->get('success_text'),
    ];
  }

  /**
   * Page which shows if donation wasn't successful.
   *
   * @return string
   *   Returns HTML string
   */
  public function fail() {
    return [
      '#type' => 'markup',
      '#markup' => $this->config('paypal.settings')->get('fail_text'),
    ];
  }
  
  public function updateUserSubscription($data, $id, $user_id) {
    if (empty($data) || empty($id) || empty($user_id)) {
      return;
    }
    db_update('user_subscription')
        ->fields($data)
        ->condition('sid', $id)
        ->condition('user_id', $user_id)
        ->condition('status', 0)
        ->condition('payment_status', 0)
        ->execute();
  }

}