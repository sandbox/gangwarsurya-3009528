<?php

namespace Drupal\registration_subscription;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Subscription entity.
 *
 * @ingroup registration_subscription
 */
interface SubscriptionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
