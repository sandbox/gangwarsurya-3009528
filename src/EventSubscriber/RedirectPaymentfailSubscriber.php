<?php

namespace Drupal\registration_subscription\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectPaymentfailSubscriber implements EventSubscriberInterface {

  /**
   * Check route.
   */
  public function checkForcePayment(GetResponseEvent $event) {

    global $base_url;
    $roles = \Drupal::config('tfa.settings')->get('required_roles');
    $current_user = \Drupal::currentUser();
    $user_roles = $current_user->getRoles();
    $current_role = $user_role[1];
    $route_name = \Drupal::routeMatch()->getRouteName();

    //Get user subscription details
    $user_sub_payment_status = \Drupal::entityTypeManager()->getStorage('user_subscription')
          ->loadByProperties(['user_id' => $current_user->id(), 'payment_status' => 0]);
    $user_sub_payment_status = reset($user_sub_payment_status);
    if (isset($user_sub_payment_status->sid)) {
      $user_subscription_id = $user_sub_payment_status->get('sid')->getValue()[0]['target_id'];
    }

    //Get Package Information
    if (!empty($user_subscription_id)) {
      $package_info = \Drupal::entityTypeManager()->getStorage('subscription')
            ->loadByProperties(['id' => $user_subscription_id]);
      $package_info = reset($package_info);
      if (isset($package_info->subscription_amount)) {
        $subscription_amount = $package_info->get('subscription_amount')->getValue()[0]['value'];
      }
    }
    if ($current_role != 'administrator' && $user_subscription_id > 0 && $subscription_amount > 0) {
      $uid = $current_user->id();
      $exclude_pages = ['user.logout', 'registration_subscription.payment_form', 
        'registration_subscription.return_page_controller_fail', 'registration_subscription.return_page_controller_success'];
      if (!in_array($route_name, $exclude_pages)) {
        $redirect = new RedirectResponse($base_url . '/registration_subscription/form/payment_from');
        $redirect->send();
      }
    }
  }

  /**
   * Check function.
   */
  public static function getSubscribedEvents() {
    $route_name = \Drupal::routeMatch()->getRouteName();
     $events[KernelEvents::REQUEST][] = ['checkForcePayment'];
     return $events; 
  }

}
