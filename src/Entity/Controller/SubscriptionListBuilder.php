<?php

namespace Drupal\registration_subscription\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for subscription entity.
 *
 * @ingroup subscription
 */
class SubscriptionListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Subscription Entity implements a registration Subscription. These Subscription are fieldable entities. You can manage the fields on the <a href="@adminlink">subscription admin page</a>.', [
        '@adminlink' => \Drupal::urlGenerator()
          ->generateFromRoute('registration_subscription.subscription_settings'),
      ]),
    ];

    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the contact list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['duration'] = $this->t('Duration (Day\'s)');
    $header['type'] = $this->t('Type');
    $header['role'] = $this->t('Role');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\content_entity_example\Entity\Contact */
    $row['id'] = $entity->id();
    $row['title'] = $entity->link();
    $row['duration'] = $entity->duration->value;
    $row['type'] = ucfirst($entity->type->value);
    $row['role'] = ucfirst($entity->role->value);
    return $row + parent::buildRow($entity);
  }

}
