<?php

namespace Drupal\registration_subscription\Form;

use Drupal\registration_subscription\Entity\Subscription;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RegistrationSubscriptionForm.
 */
class RegistrationSubscriptionForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'registration_subscription_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $subscriptions = $this->getSubscriptionDetails();
    $form['subscription']['#attached']['library'][] = 'registration_subscription/registration_subscription_form';
    $form['subscription']['#prefix'] = '<ul id="products" class="grid clearfix">';
    $i = 0;
    foreach ($subscriptions as $key => $value) {
      $value['duration'] = ($value['duration'] == '-1') ? 'Unlimted' : $value['duration'];
      $form['subscription'][$i] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Subscription'),
        '#weight' => '0',
        '#prefix' => '<li class="clearfix">
                    <section class="left">
                        <h3>' . $value['title'] . '</h3>
                        <span class="meta">Duration: ' . $value['duration'] . ' Day\'s</span>
                    </section>',
        '#suffix' => '<section class="right">
                        <span class="price">$' . $value['amount'] . '</span>
                    </section>
                </li>',
        '#weight' => '99',
      ];
      $form['subscription'][$i]['#attributes']['class'][] = 'subject-list';
      $i++;
    }
    $form['subscription']['#suffix'] = '</ul>';
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      drupal_set_message($key . ': ' . $value);
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getSubscriptionDetails() {
    $subs_details = Subscription::loadMultiple();
    $data = [];
    if (count($subs_details) <= 0) {
      return $data;
    }

    foreach ($subs_details as $key => $val) {
      $data[$key]['title'] = $val->get('title')->getValue()[0]['value'];
      $data[$key]['amount'] = $val->get('subscription_amount')->getValue()[0]['value'];
      $data[$key]['duration'] = $val->get('duration')->getValue()[0]['value'];
      $data[$key]['description'] = $val->get('description')->getValue()[0]['value'];
      $data[$key]['subscription_id'] = $val->id();
      $data[$key]['unlimited_subscription'] = $val->get('unlimited_subscription')->getValue()[0]['value'];
    }
    return $data;
  }

}
