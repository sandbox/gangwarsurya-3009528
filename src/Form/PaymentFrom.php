<?php

namespace Drupal\registration_subscription\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\Url;
use Drupal\facets\Exception\Exception;
use Drupal\paypal\Configuration;
use Drupal\user\PrivateTempStoreFactory;
use PayPal\CoreComponentTypes\BasicAmountType;
use PayPal\EBLBaseComponents\BillingAgreementDetailsType;
use PayPal\EBLBaseComponents\PaymentDetailsItemType;
use PayPal\EBLBaseComponents\PaymentDetailsType;
use PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType;
use PayPal\PayPalAPI\SetExpressCheckoutReq;
use PayPal\PayPalAPI\SetExpressCheckoutRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\registration_subscription\Entity\Subscription;

/**
 * Class PaymentFrom.
 */
class PaymentFrom extends FormBase {

  protected $tempStore;
  protected $sessionManager;
  protected $currentUser;

  /**
   * DonateForm constructor.
   *
   * @param \Drupal\paypal\Form\PrivateTempStoreFactory $temp_store_factory
   *   User's temp store.
   * @param \Drupal\paypal\Form\SessionManagerInterface $session_manager
   *   User's session manager.
   * @param \Drupal\paypal\Form\AccountInterface $current_user
   *   Currently logged in user.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_factory, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->tempStore = $temp_store_factory->get('paypal');
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('user.private_tempstore'), $container->get('session_manager'), $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $uid = \Drupal::currentUser()->id();
    if (!empty($uid)) {
      $user_subscription = db_select('user_subscription', 'us')
                  ->fields('us')->condition('user_id', $uid)
                  ->condition('status', 0)
                  ->condition('payment_status', 0)
                  ->execute()->fetchAssoc();
      $subscription_id = $user_subscription['sid'];
      if (!empty($subscription_id)) {
        $subscription = Subscription::load($subscription_id);
        $subscription_amount = $subscription->get('subscription_amount')->getValue()[0]['value'];
      }
    }

    $form['subscription_amount'] = [
      '#type' => 'hidden',
      '#default_value' => $subscription_amount,
    ];
    
    $form['subscription_id'] = [
      '#type' => 'hidden',
      '#default_value' => $user_subscription['sid'],
    ];
    $form['subscription_user_id'] = [
      '#type' => 'hidden',
      '#default_value' => $uid,
    ];
    
    $form['payment_options'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => 'Payment Options',
    ];
    $form['payment_options']['payment'] = [
        '#type' => 'radios',
        //'#title' => $this->t('Payment Options'),
        //'#description' => $this->t('payment method paypal'),
        '#required' => TRUE,
        '#options' => [
            'paypal' => $this->t('Paypal'),
            'authorizednet' => $this->t('Authorized.net'),
            'payumoney' => $this->t('PayUmoney'),
        ],
        '#weight' => '0',
    ];
    $form['payment_options']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Paynow'),
    ];
    $form['payment_options']['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel'),
        '#submit' => ['::submitCancelForm'],
        '#limit_validation_errors' => [],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

    /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;
    $config = $this->config('paypal.settings');
    $params = [];
    // Getting donation amount either from radios or from a custom text field.
    if ($amount_custom = $form_state->getValue('subscription_amount')) {
      $params['amount'] = $amount_custom;
    }
    $subscription_id = $form_state->getValue('subscription_id');
    $subscription_user_id = $form_state->getValue('subscription_user_id');     
    $sandbox = "";
    if ($config->get('sandbox')) {
      $sandbox = 'sandbox.';
    }
    $params['cancel_return'] = $base_url . base_path() . Url::fromRoute('registration_subscription.return_page_controller_fail')->toString();
    if ($form_state->getValue('recurring')) {
      $params['return'] = $base_url . base_path() . Url::fromRoute('paypal.recurring_controller_init')->toString();
      $paymentDetails = new PaymentDetailsType();
      $itemAmount = new BasicAmountType($config->get('currency_code'), $params['amount']);
      $itemDetails = new PaymentDetailsItemType();
      $itemDetails->Name = (string) $this->t("Donation");
      $itemDetails->Amount = $itemAmount;
      $itemDetails->Quantity = 1;
      $itemDetails->ItemCategory = "Digital";
      $paymentDetails->PaymentDetailsItem[0] = $itemDetails;

      $paymentDetails->ItemTotal = new BasicAmountType($config->get('currency_code'), $params['amount']);
      $paymentDetails->OrderTotal = new BasicAmountType($config->get('currency_code'), $params['amount']);

      $setECReqDetails = new SetExpressCheckoutRequestDetailsType();
      $setECReqDetails->PaymentDetails = $paymentDetails;
      $setECReqDetails->CancelURL = $params['cancel_return'];
      $setECReqDetails->ReturnURL = $params['return'];
      $setECReqDetails->SolutionType = 'Sole';
      $setECReqDetails->NoShipping = 1;
      $setECReqDetails->AddressOverride = 0;
      $setECReqDetails->ReqConfirmShipping = 0;

      $billingAgreementDetails = new BillingAgreementDetailsType("RecurringPayments");
      $billingAgreementDetails->BillingAgreementDescription = $config->get('billing_description');
      $setECReqDetails->BillingAgreementDetails = [$billingAgreementDetails];

      $setECReqType = new SetExpressCheckoutRequestType();
      $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
      $setECReq = new SetExpressCheckoutReq();
      $setECReq->SetExpressCheckoutRequest = $setECReqType;

      $paypalService = new PayPalAPIInterfaceServiceService(Configuration::getConfig());

      try {
        $setECResponse = $paypalService->SetExpressCheckout($setECReq);
      }
      catch (Exception $ex) {
        throw new \Exception("PayPal SetExpressCheckout: " . $ex);
      }
      if (isset($setECResponse)) {
        if ($setECResponse->Ack == 'Success') {
          $token = $setECResponse->Token;
          // Setting extra parameters.
          $extra_params = [
            'recurring_options' => $form_state->getValue('recurring_options'),
            'recurring_cycles' => $form_state->getValue('recurring_cycles'),
            'amount' => $params['amount'],
          ];
          // If user is anonymous, session needs to be manually started.
          if ($this->currentUser->isAnonymous() && !isset($_SESSION['session_started'])) {
            $_SESSION['session_started'] = TRUE;
            $this->sessionManager->start();
          }
          // Saving extra parameters in user's tempstore. When user comes back
          // from Paypal's checkout to RecurringPaymentController, data from the
          // Drupal donation form is lost. Hence this data is saved here in user
          // private tempstore so it can be restored in
          // RecurringPaymentController.
          $this->tempStore->set('extra_params', $extra_params);
          $url = 'https://www.' . $sandbox . 'paypal.com/webscr?cmd=_express-checkout&token=' . $token;
        }
        else {
          throw new \Exception($setECResponse->Errors[0]['ShortMessage'] . ' Error Code: ' . $setECResponse->Errors[0]['ErrorCode']);
        }
      }
    }
    else {
      // Adding mandatory params for PayPal.
      $params['return'] = $base_url . base_path() . Url::fromRoute('registration_subscription.return_page_controller_success')->toString();
      //$params['return'] = 'http://drupal-cms.com/paypal.php';
      $params['cmd'] = '_xclick';
      $params['custom'] = $subscription_id . '@@@' . $subscription_user_id;
      $params['business'] = $config->get('business');
      $params['lc'] = $config->get('lc');
      $params['item_name'] = $config->get('item_name');
      $params['item_number'] = $config->get('item_number');
      $params['currency_code'] = $config->get('currency_code');
      $params['no_note'] = 0;
      $params['cn'] = $config->get('cn');
      $params['no_shipping'] = $config->get('no_shipping');
      $params['rm'] = 2;
      $url = "https://www." . $sandbox . "paypal.com/cgi-bin/webscr" . '?' . http_build_query($params);
      //echo $url; die;
      $response = new TrustedRedirectResponse($url);
      $form_state->setResponse($response);
    }
    $response = new TrustedRedirectResponse($url);
    $form_state->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function submitCancelForm(array &$form, FormStateInterface $form_state) {
    $url = new Url('user.register');
    $form_state->setRedirectUrl($url);
  }

}
