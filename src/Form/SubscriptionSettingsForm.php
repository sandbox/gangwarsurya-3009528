<?php

namespace Drupal\registration_subscription\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SubscriptionSettingsForm.
 *
 * @package Drupal\registration_subscription\Form
 * @ingroup registration_subscription
 */
class SubscriptionSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  protected function getEditableConfigNames() {
    return [
      'registration_subscription.subscriptionsettings',
    ];
  }

  /**
   *
   */
  public function getFormId() {
    return 'registration_subscription_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('registration_subscription.subscriptionsettings')
      ->set('subscription_email', $form_state->getValue('subscription_email'))
      ->set('notification_days_for_renewal', $form_state->getValue('notification_days_for_renewal'))
      ->set('subscription_renewal_notification_email_template', $form_state->getValue('subscription_renewal_notification_email_template'))
      ->set('subscription_expire_email_template', $form_state->getValue('subscription_expire_email_template'))
      ->set('show_subscription', $form_state->getValue('show_subscription'))
      ->set('currency', $form_state->getValue('currency'))
      ->set('show_settings', $form_state->getValue('show_settings'))
      ->save();
  }

  /**
   * Define the form used for SubscriptionSettingsForm settings.
   *
   * @return array
   *   Form definition array.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('registration_subscription.subscriptionsettings');
    $form['subscription_settings_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Subscription Settings Details'),

    ];
    $form['subscription_settings_fieldset']['subscription_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subscription Email'),
      '#default_value' => $config->get('subscription_email'),
    ];
    $form['subscription_settings_fieldset']['notification_days_for_renewal'] = [
      '#type' => 'number',
      '#title' => $this->t('Notification Days For Renewal'),
      '#required' => TRUE,
      '#default_value' => $config->get('notification_days_for_renewal'),
    ];
    $form['subscription_settings_fieldset']['subscription_renewal_notification_email_template'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Subscription Renewal Notification Email Template'),
      '#default_value' => $config->get('subscription_renewal_notification_email_template')['value'],
    ];
    $form['subscription_settings_fieldset']['subscription_expire_email_template'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Subscription Expire Email Template'),
      '#default_value' => $config->get('subscription_expire_email_template')['value'],
    ];
    $form['subscription_settings_fieldset']['currency'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Currency'),
      '#default_value' => $config->get('currency'),
      '#required' => TRUE,
      '#description' => t('country currency'),
    ];
    $form['subscription_settings_fieldset']['show_subscription'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Subscription'),
      '#default_value' => $config->get('show_subscription'),
      '#required' => TRUE,
      '#options' => [
        t('Registration Form'),
        t('After Registration'),
      ],
    ];
    $form['subscription_settings_fieldset']['show_settings'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Settings'),
      '#default_value' => $config->get('show_settings'),
      '#required' => TRUE,
      '#options' => [
        t('Grid'),
        t('List'),
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $subscription_email = $form_state->getValue('subscription_email');
    $is_valid_email = \Drupal::service('email.validator')->isValid($subscription_email);
    if ($is_valid_email == FALSE) {
      $form_state->setErrorByName('subscription_email', t('Invalid Email.'));
    }

    parent::validateForm($form, $form_state);
  }

}
