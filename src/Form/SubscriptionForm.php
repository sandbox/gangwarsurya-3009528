<?php

namespace Drupal\registration_subscription\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the registration_subscription entity edit forms.
 *
 * @ingroup registration_subscription
 */
class SubscriptionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\registration_subscription\Entity\Subscription */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;
    $form['#attributes'] = ['id' => 'ajax-wrapper'];
    $form['langcode'] = [
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
    ];
    if ($entity->id() > 0 && $entity->type->value == 'free') {
      $form['duration']['#access'] = FALSE;
      $form['subscription_amount']['#access'] = FALSE;
      $form['duration']['widget']['0']['value']['#required'] = FALSE;
      $form['subscription_amount']['widget']['0']['value']['#required'] = FALSE;
    }
    $form['title']['widget']['0']['value']['#required'] = TRUE;
    $form['type']['widget']['#required'] = TRUE;
    $form['role']['widget']['#required'] = TRUE;

    $form['type']['widget']['#ajax'] = [
      'callback' => '::freeSubscriptionCallback',
      'wrapper' => 'ajax-wrapper',
      'effect' => 'fade',
      'event' => 'change',
      'progress' => [
        'type' => 'throbber',
        'message' => NULL,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if ($form_state->getValue('unlimited_subscription')['value']) {
      $form_state->setValue('duration', [['value' => '-1']]);
      $form_state->setValue('subscription_amount', [['value' => '0']]);
    }
    if ($form_state->getValue('type')[0]['value'] == 'free') {
      $form['unlimited_subscription']['#access'] = FALSE;
      $form['unlimited_subscription']['widget']['0']['value']['#required'] = FALSE;
      $form['duration']['#access'] = FALSE;
      $form['subscription_amount']['#access'] = FALSE;
      $form['duration']['widget']['0']['value']['#required'] = FALSE;
      $form['subscription_amount']['widget']['0']['value']['#required'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function freeSubscriptionCallback(array $form, FormStateInterface $form_state) {

    if ($form_state->getValue('type')[0]['value'] == 'free') {
      $form['duration']['#access'] = FALSE;
      $form['subscription_amount']['#access'] = FALSE;
      $form['unlimited_subscription']['widget']['value']['#required'] = TRUE;
      $form['duration']['widget']['0']['value']['#required'] = FALSE;
      $form['subscription_amount']['widget']['0']['value']['#required'] = FALSE;
      $form_state->setRebuild(TRUE);
    }

    if ($form_state->getValue('type')[0]['value'] == 'paid') {
      $form['unlimited_subscription']['#access'] = FALSE;
      $form['unlimited_subscription']['widget']['0']['value']['#required'] = TRUE;
      $form['duration']['widget']['0']['value']['#required'] = TRUE;
      $form['subscription_amount']['widget']['0']['value']['#required'] = TRUE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('The contact %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));
    }
    else {
      drupal_set_message($this->t('The contact %feed has been added.', ['%feed' => $entity->toLink()->toString()]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

}
