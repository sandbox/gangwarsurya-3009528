<?php

namespace Drupal\registration_subscription\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SubscriptionBlock' block.
 *
 * @Block(
 *  id = "subscription_block",
 *  admin_label = @Translation("Subscription block"),
 * )
 */
class SubscriptionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['subscription_block_subscription']['form'] = \Drupal::formBuilder()->getForm('Drupal\registration_subscription\Form\RegistrationSubscriptionForm');
    return $build;
  }

}
